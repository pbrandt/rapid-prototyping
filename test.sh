#!/usr/bin/env bash

echo "running tests"

set -e

echo "installing dependencies"
npm i

echo "running npm run test"
npm run test

