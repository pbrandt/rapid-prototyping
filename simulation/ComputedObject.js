const RESERVED = ['add', 'compute']

const debug = function () {} ; // console.log.bind(console);

/**
 * Class that is an object where every property is a function
 */
class ComputedObject {
    constructor(obj) {
        this.add(obj || {})
    }

    add(tag, obj) {
        if (typeof tag !== 'string') {
            obj = tag;
            tag = ''
        }

        Object.getOwnPropertyNames(obj).map(key => {
            // debug(key, typeof obj[key]);

            // Check for dumb mistakes
            if (RESERVED.includes(key)) {
                throw new Error('cannot add property "' + key + '" to a ComputedObject')
            }

            if (this[key] instanceof ComputedObject) {
                this[key].add(tag, obj[key])
            }

          else if (typeof this[key] === 'undefined') {
              // first add all new properties
            if (typeof obj[key] === 'number') {
              this[key] = () => obj[key]
            } else if (typeof obj[key] === 'function') {
              this[key] = obj[key]
            } else if (typeof obj[key] === 'object') {
                // object or array
                this[key] = new ComputedObject();
                this[key].add(tag, obj[key])
            } else {
              throw new Error('unexpected type supplied to add for obj[' + key + ']')
            }
          } else if (typeof this[key] === 'function') {
              // then extend the exising function with the new one
            var existing_fn = this[key]
            var added_fn
    
            if (typeof obj[key] === 'number') {
              added_fn = () => obj[key]
            } else if (typeof obj[key] === 'function') {
              added_fn = obj[key]
            } else {
              throw new Error('unexpected type supplied to add for obj[' + key + ']')
            }
    
            /**
             * Then new function preserves arguments and the "this" context of the calling function
             * this is what makes the derivative stuff work with like vx() { -0.1 * this.x }
             */
            this[key] = function() {
                return existing_fn.apply(this, arguments) + added_fn.apply(this, arguments)
            }
          } else {
            throw new Error('unexpected type, expected this[' + key + '] to be undefined or function but was somehow magically ' + typeof this[key])
          }
        })
    }

    /**
     * compute the value of this function by executing each leaf node function with the 
     * @param {*} state 
     * @param {*} t 
     */
    compute(state, t) {
        var computed = Object.getOwnPropertyNames(this).reduce( (computed, k) => {
            if (RESERVED.includes(k)) return computed

            if (this[k] instanceof ComputedObject) {
                // not leaf node
                debug('recursing into', k)
                computed[k] = this[k].compute(state, t)
            } else {
                // should be leaf node
                computed[k] = this[k](state, t)
                if (isNaN(computed[k])) {
                    debug('NaN for property', k)
                    debug('state', state)
                    debug('t', t)
                    throw new Error('computed NaN for derivative on property ' + k)
                }
            }
            return computed
        }, {})
        debug('computed derivative:', computed)
        return computed
    }
}

module.exports = ComputedObject

if (!module.parent) {
    var assert = require('assert')
    var o = new ComputedObject({
        x: 10
    })

    o.add({
        x: () => 1
    })

    var x = o.x();
    assert(x === 11)

    var oc = o.compute()
    assert(oc.x === 11)

    // lets try with this
    var state = { x: 1}
    var deriv = { vx: function (factor) {
        console.log(this)
        return this.x * factor
    } } // NOTE you CANNOT do vx: () => this.x * 0.1; because "this" is not bound
    deriv = new ComputedObject(deriv)
    var d = deriv.bind_and_compute(state, 0.1)
    assert(d.vx === 0.1)

    console.log('test passed')
}
