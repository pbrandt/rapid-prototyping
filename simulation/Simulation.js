const Interface = require('../interface')
// const ComputedObject = require('./ComputedObject')
const Derivative = require('./Derivative')

/**
 * this is what people instantiate to build a simulation
 */
class Simulation {
  constructor () {
    this.state = {}
    this.derivatives = new Derivative()
    this.data = []
    this.t = 0
  }

  step (dt) {
    // possibly log the very first state
    if (this.data.length == 0) {
      this.data.push({
        state: Object.assign({}, this.state),
        derivatives: this.derivatives.compute_by_tags(this.state, this.t),
        t: this.t
      })
    }

    // pretty ok timestep
    dt = dt || this.dt || 0.01;

    // Runge-Kutta 4
    var f1 = get_derivative(this.state, this.derivatives, this.t)
    var k1 = multiply(f1, dt)

    var f2 = get_derivative(add(this.state, multiply(k1, 0.5)), this.derivatives, this.t + dt / 2)
    var k2 = multiply(f2, dt)

    var f3 = get_derivative(add(this.state, multiply(k2, 0.5)), this.derivatives, this.t + dt / 2)
    var k3 = multiply(f3, dt)

    var f4 = get_derivative(add(this.state, k3), this.derivatives, this.t + dt)
    var k4 = multiply(f4, dt)

    var next_state = add(this.state, multiply(add(k1, add(multiply(k2, 2), add(multiply(k3, 2), k4))), 1/6))

    // set this state
    Object.assign(this.state, next_state)
    this.t += dt

    // log this new state
    this.data.push({
      state: next_state,
      derivatives: this.derivatives.compute_by_tags(next_state, this.t),
      t: this.t
    })
  }

  // use case: drive your rover around in the sim with your joystick
  // this is realtime
  run_realtime () {
    if (this._running) {
      throw new Error('Simulation already running')
    }
    this._running = true

    /** using arrow functions so i can keep lexical this */
    var last_ts = Date.now();
    const run_step = () => {
      var ts = Date.now();
      this.step(ts - last_ts)
      last_ts = ts;
      if (this._running) {
        setImmediate(() => {
          run_step()
        })
      }
    }
    run_step()
  }

  step_realtime() {
    var hrtime = process.hrtime();
    var ts = hrtime[0] + hrtime[1] / 1e9;

    if (this.last_ts) {
      this.step((ts - this.last_ts))
      this.last_ts = ts;

      // send data to webz
      if (this.interface) {
        this.interface.push_data(this.data.slice(-1))
      }
    } else {
      this.last_ts = ts;
    }
  }

  stop () {
    this._running = false
  }

  async start_interface (port) {
    port = port || 8080
    this.interface = new Interface(port, this)
    await this.interface.ready
    return this.interface
  }
}

module.exports = Simulation

// HELPER FUNCTIONS BELOW:

/**
 * calculates the derivative at time t using the state and derivatives function
 */
function get_derivative (state, derivatives, t) {
  var computed_derivatives = derivatives.compute(state, t)

  return computed_derivatives
}

/**
 * multiply a deep object state by a constant value. each leaf node should get multiplied
 * @param {*} state 
 * @param {*} constant 
 */
function multiply(state, constant) {
  return Object.getOwnPropertyNames(state).reduce((o, k) => {
    if (typeof state[k] === 'object') {
      // not leaf node
      o[k] = multiply(state[k], constant)
    } else if (typeof state[k] === 'number') {
      // leaf node
      o[k] = state[k] * constant;
    }

    return o
  }, {})
}

// function add_constant(state, constant) {
//   return Object.getOwnPropertyNames(state).reduce((o, k) => {
//     o[k] = state[k] + constant;
//     return o
//   }, {})
// }

/**
 * add two deep objects together.
 * note that a could have properties that b does not have, and vice versa
 * @param {*} a
 * @param {*} b
 */
function add(a, b) {
  // first go thru a's properties
  var ret = Object.getOwnPropertyNames(a).reduce((o, k) => {
    if (typeof a[k] === 'object') {
      // def not a leaf node. also note that b[k] might not exist, so safeguard with {}
      o[k] = add(a[k], b[k] || {})
    } else if (typeof a[k] === 'number' && typeof b[k] === 'number') {
      // leaf node
      o[k] = a[k] + b[k];
    } else if (typeof a[k] === 'number' && typeof b[k] === 'undefined') {
      o[k] = a[k]
    } else {
      throw new Error('unexpected types for property ' + k)
    }
    return o
  }, {})

  // next go thru b's properties to find ones in b that are not in a
  Object.getOwnPropertyNames(b).map((k) => {
    // skip all properties caught by the first loop
    if (typeof ret[k] === 'undefined') {
      // no need to recurse because a should not have any of these properties
      ret[k] = b[k]
    }
  })
  return ret
}
