const rp = require('..')

const default_state = {
  // position
  x: 0,
  y: 0,
  z: 0,

  // velocity
  vx: 0,
  vy: 0,
  vz: 0,

  // quaternion rotation
  w: 0,
  v1: 1,
  v2: 0,
  v3: 0,

  // angular rotation rate
  dw: 0,
  dv1: 1,
  dv2: 0,
  dv3: 0
}

const default_state_deriv = {
  x: function () { return this.vx },
  y: function () { return this.vy },
  z: function () { return this.vz },
  vx: 0,
  vy: 0,
  vz: 0
}

/**
 * There's got to be a better way to do this
 */
class RigidBody {
  constructor(state, deriv) {
    var self = this; // ye olde javascript idion

    // start with defaults and then add in the state argument values
    this.state = Object.assign({}, default_state)
    Object.assign(this.state, state || {})

    // similarly start with defaults, but then ADD in the deriv argument stuff
    this.deriv = new rp.ComputedObject(default_state_deriv)
    this.deriv.add(deriv || {})

    // and shim the state into the deriv
    this.deriv.compute = function() {
      return self.deriv.bind_and_compute(self.state, arguments)
    }
  }
}

module.exports = RigidBody;

if (!module.parent) {
  var b1 = new RigidBody({vx: 10})
  var b2 = new RigidBody({x: 10}, {vx: 1})
  var b3 = new RigidBody();
  console.log(b1.state)
  console.log(b1.deriv.compute())
  console.log(b2.state)
  console.log(b2.deriv.compute())
  console.log(b3.state)
  console.log(b3.deriv.compute())
}