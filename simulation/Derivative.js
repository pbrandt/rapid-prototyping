const RESERVED = ['add', 'compute', '_derivatives_list', '_derivatives_counter', 'compute_by_tag']

const debug = function () {} ; // console.log.bind(console);
// const debug = console.log.bind(console)


/**
 * Class that contains the logic for computing derivatives.
 */
class Derivative {
    constructor(obj) {
        this._derivatives_list = [];
        this._derivatives_counter = 1;
        if (typeof obj !== 'undefined') {
            this.add(obj);
        }
    }

    add(tag, obj) {
        if (typeof tag !== 'string') {
            obj = tag;
            tag = 'derivative ' + this._derivatives_counter;
            this._derivatives_counter++;
        }

        this._derivatives_list.push({
            tag: tag,
            obj: obj
        });
    }

    /**
     * compute the value of this function by executing each leaf node function with the state and time.
     * adding all registered derivatives together
     * @param {*} state 
     * @param {*} t 
     */
    compute(state, t) {
        return this._derivatives_list.reduce( (result, deriv) => {
            var d = recursive_call_function(deriv.obj, state, t);
            return add(result, d);
        }, {})
    }

    /**
     * compute all derivitaves, but split by tags
     * @param {*} state 
     * @param {*} t 
     */
    compute_by_tags(state, t) {
      debug('compute by state')
      debug(state)
      debug(this._derivatives_list[0].obj)
        return this._derivatives_list.reduce( (result, deriv) => {
            var d = recursive_call_function(deriv.obj, state, t);
            debug(d)
            result[deriv.tag] = d;
            return result
        }, {})
    }
}

module.exports = Derivative

if (!module.parent) {
    var assert = require('assert')
    var o = new Derivative({
        x: 10
    })

    o.add({
        x: () => 1
    })

    var x = o.x();
    assert(x === 11)

    var oc = o.compute()
    assert(oc.x === 11)

    // lets try with this
    var state = { x: 1}
    var deriv = { vx: function (factor) {
        console.log(this)
        return this.x * factor
    } } // NOTE you CANNOT do vx: () => this.x * 0.1; because "this" is not bound
    deriv = new Derivative(deriv)
    var d = deriv.bind_and_compute(state, 0.1)
    assert(d.vx === 0.1)

    console.log('test passed')
}


/**
 * add two deep objects together.
 * note that a could have properties that b does not have, and vice versa
 * @param {*} a
 * @param {*} b
 */
function add(a, b) {
    // first go thru a's properties
    var ret = Object.getOwnPropertyNames(a).reduce((o, k) => {
      if (typeof a[k] === 'object') {
        // def not a leaf node. also note that b[k] might not exist, so safeguard with {}
        o[k] = add(a[k], b[k] || {})
      } else if (typeof a[k] === 'number' && typeof b[k] === 'number') {
        // leaf node
        o[k] = a[k] + b[k];
      } else if (typeof a[k] === 'number' && typeof b[k] === 'undefined') {
        o[k] = a[k]
      } else {
        throw new Error('unexpected types for property ' + k)
      }
      return o
    }, {})
  
    // next go thru b's properties to find ones in b that are not in a
    Object.getOwnPropertyNames(b).map((k) => {
      // skip all properties caught by the first loop
      if (typeof ret[k] === 'undefined') {
        // no need to recurse because a should not have any of these properties
        ret[k] = b[k]
      }
    })
    return ret
  }
  
  function recursive_call_function(obj, state, t) {
    var props
    if (obj instanceof Array) {
      props = Object.keys(obj)
    } else {
      props = Object.getOwnPropertyNames(obj)
    }
    var computed = props.reduce( (computed, k) => {
        if (RESERVED.includes(k)) return computed

        if (typeof obj[k] === 'object') {
            // not leaf node
            debug('recursing into', k)
            computed[k] = recursive_call_function(obj[k], state, t)
        } else {
            // should be leaf node
            debug('leaf node', k)
            computed[k] = obj[k](state, t)
            if (isNaN(computed[k])) {
                debug('NaN for property', k)
                debug('state', state)
                debug('t', t)
                throw new Error('computed NaN for derivative on property ' + k)
            }
        }
        return computed
    }, {})
    debug('computed derivative:', computed)
    return computed
  }
