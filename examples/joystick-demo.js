const rp = require('..')
const sim = new rp.Simulation()
const joystick = new rp.Joystick()

/**
 * Two-dimensional simulation
 */
const state = {
  x: 0,
  y: 0,
  vx: 0,
  vy: 0.001
}

/**
 * derivatives in 2d
 */
const state_deriv = {
  x: state => state.vx,
  y: state => state.vy,
  vx: state => {
    return joystick.roll
  },
  vy: state => {
    return joystick.pitch
  }
}

sim.setup(state, state_deriv)
console.log(sim.state)

/**
 * Set up periodic logging
 */
setInterval(_ => {
  console.log(sim.state)
}, 1000)

/**
 * main function
 */
async function main () {
  await sim.run_until((state, t) => {
    return state.y >= 2
  })

  console.log('hit 2, let\'s add some drag')
  console.log(sim.state)
  state_deriv.vy = (state, t) => {
    var velocity = Math.sqrt(state.vx * state.vx + state.vy * state.vy)
    var cd = 0.1
    var total_drag = cd * velocity * velocity
    var vy_drag = -1 * state.vy / velocity * total_drag
    return vy_drag
  }

  sim.setup(sim.state, state_deriv)

  await sim.run_until((state, t) => {
    return state.y >= 4
  })
  console.log('hit 4')
}

main()
