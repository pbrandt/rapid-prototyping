const rp = require('..')
const sim = new rp.Simulation()
const RigidBody = require('../simulation/RigidBody')

/**
 * Two-dimensional simulation of a cannonball shot at 45 degrees
 */
const cannonball = new RigidBody()

cannonball.state.vx = 250 * Math.cos(Math.PI/4)
cannonball.state.vy = 250 * Math.sin(Math.PI/4),

// Add gravity
cannonball.deriv.add({
    vy: -9.81
})

// Add drag
const cd = 0.5 // coefficient of drag for a sphere
const A = 0.1 * 0.1 * Math.PI / 4; // 10 cm diameter cannonball
const atmospheric_density = 1.225
cannonball.deriv.add({
    vx() {
        var speed = Math.sqrt(this.vx * this.vx + this.vy * this.vy)
        return - 1/2 * atmospheric_density * speed * cd * A * this.vx
    },

    vy() {
        var speed = Math.sqrt(this.vx * this.vx + this.vy * this.vy)
        return - 1/2 * atmospheric_density * speed * cd * A * this.vy
    }
})

/**
 * register with the sim
 */
sim.state = cannonball.state
sim.deriv = cannonball.deriv

/**
 * main function
 */
async function main() {
    var interface = sim.start_interface(8080)
    sim.step()
    sim.step()
    sim.step()
    await sim.run_until(state => state.y < 0)
    interface.send_data(sim.data)
    interface.plot('x', 'y')
    interface.plot('t', ['x', 'vx'])
    interface.plot('t', ['y', 'vy'])
    interface.draw()
}

main()

