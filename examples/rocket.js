/** testing a rigid body */
var rp = require('..')
var sim = new rp.Simulation()

// coordinate frames make things more practical
var earth = new sim.PlanetaryBody.Earth()
var rocket = new sim.RigidBody('rocket')

// what's point if you can't even have fun
rocket.set_position(earth.historic_launchpad_39_A)
rocket.set_orientation(earth.pointing_up)

// gravity is already taken care of.
// just need power
// x axis is the one along the rocket body, with +x pointing at the nose.
// so thrust is in the -x direction
// maybe add a gimbal later
thrust = {
  vx: state => {
    return -1000;
  }
}
rocket.add_deriv(thrust)

// launch it
// let it run until it's at 100 km orbit
while (rocket.radar_distance_from(earth) <= earth.radius.km + 100.km) {
  sim.step()
}
console.log(sim.state)
