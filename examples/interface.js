const rp = require('..')
const sim = new rp.Simulation()

/**
 * Two-dimensional simulation of a cannonball shot at 45 degrees
 */
const state = {
    x: 0,
    y: 0,
    vx: 250 * Math.cos(Math.PI/4),
    vy: 250 * Math.sin(Math.PI/4)
}

/**
 * derivatives in 2d
 */
const state_deriv = {
    x: state => state.vx,
    y: state => state.vy
}

const gravity = {
    vy: -9.81
}

const cd = 0.5 // coefficient of drag for a sphere
const A = 0.1 * 0.1 * Math.PI / 4; // 10 cm diameter cannonball
const atmospheric_density = 1.225
const drag = {
    vx: state => {
        var speed = Math.sqrt(state.vx * state.vx + state.vy * state.vy)
        return - 1/2 * atmospheric_density * speed * cd * A * state.vx
    },

    vy: state => {
        var speed = Math.sqrt(state.vx * state.vx + state.vy * state.vy)
        return - 1/2 * atmospheric_density * speed * cd * A * state.vy
    }
}

/**
 * Setup
 */
sim.set_state(state)
sim.add_deriv(state_deriv)
sim.add_deriv(gravity)
sim.add_deriv(drag)

/**
 * main function
 */
async function main() {
    var interface = sim.start_interface(8080)
    await sim.run_until(state => state.y < 0)
    interface.send_data(sim.data)
    interface.plot('x', 'y')
    interface.plot('t', ['x', 'vx'])
    interface.plot('t', ['y', 'vy'])
}

main()

