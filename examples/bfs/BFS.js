
/** BFS constants */
var MAX_THRUST = 1234;
var MIN_FUEL = 1;
var MAX_FUEL = 1000;
var EMPTY_WEIGHT = 1234;

const RigidBody = require('../../simulation/RigidBody')

/**
 * Big fucking spaceship class
 */
class BFS extends RigidBody {
  constructor() {
    // wow
    this.state.throttle = 0;
    this.state.fuel = 0;

    // gravity
    this.deriv.add({
      vz() {
        return -9.81
      }
    })

    // drag
    this.deriv.add({
      vx() {
        var speed = Math.sqrt(this.vx * this.vx + this.vy * this.vy + this.vz * this.vz)
        return - 1/2 * atmospheric_density * speed * cd * A * this.vx
      },

      vy() {
        var speed = Math.sqrt(this.vx * this.vx + this.vy * this.vy + this.vz * this.vz)
        return - 1/2 * atmospheric_density * speed * cd * A * this.vy
      },

      vz() {
        var speed = Math.sqrt(this.vx * this.vx + this.vy * this.vy + this.vz * this.vz)
        return - 1/2 * atmospheric_density * speed * cd * A * this.vz
      }
    })

    // rocket thrust
    this.deriv.add({
      vz() {
        return MAX_THRUST * this.throttle
      }
    })

  }

  /**
   * Add Fuel to the rocket
   * @param {Number} amount_kg amount of fuel to add or subtract in kg
   */
  add_fuel(amount_kg) {

  }
}

