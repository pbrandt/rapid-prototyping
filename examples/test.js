/** testing a rocket */

var state = {
  x: 0,
  y: 0,
  z: 0,
  vx: 1,
  vy: 1,
  vz: 2
}

/** only have to specify non-zero derivatives */
var state_deriv = {
  x: state => state.vx,
  y: state => state.vy,
  z: state => state.vz,
  vz: -9.81
}

/** you can get extra fancy */
class joystick {
  constructor () {
    this.pitch = 0
    this.roll = 0
    this.yaw = 0
  }

  get controls () {
    return {
      pitch,
      roll,
      yaw
    }
  }
}

var pilot_joystick = new joystick()

state_deriv.vy = function (state, t) {
  return pilot_joystick.yaw
}

var rp = require('..')
var sim = new rp.Simulation()
sim.setup(state, state_deriv)
while (sim.state.z >= 0) {
  sim.step()
}
console.log(sim.state)
