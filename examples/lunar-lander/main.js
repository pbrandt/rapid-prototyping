const Simulation = require('../..').Simulation;

const simulation = new Simulation();
simulation.state = {
    position: {
        x: 0,
        y: 1000
    },
    velocity: {
        x: 2,
        y: 0
    },
    control: {
        throttle: 0,
        angle: 0 // 0 is straight up, and the angles go clockwise because this is aerospace engineering?
    }
};

simulation.derivatives.add('first order', {
    position: {
        x(state, t) { return state.velocity.x },
        y(state, t) { return state.velocity.y }
    },
});

simulation.derivatives.add('gravity', {
    velocity: {
        y(state, t) { return -1.665; }
    }
});

simulation.derivatives.add('thrust', {
    velocity: {
        x(state, t) {
            return state.control.throttle * Math.sin(state.control.angle * Math.PI / 180)
        },
        y(state, t) {
            return -1 * state.control.throttle * Math.cos(state.control.angle * Math.PI / 180)
        }
    }
})

// run the sim
while (simulation.state.position.y > 0) {

    // first zero out horizontal velocity
    if (simulation.state.velocity.x > 0.01) {
        simulation.state.control.angle = -90; // turn opposte 90 degrees
        simulation.state.control.throttle = 1.0;
    }

    // then burn at the exact right time to zero out vertical velocity
    else if (false) {
        simulation.state.controls.angle = 90;
    } else {
      simulation.state.control.throttle = 0;
    }
    console.log('stepping')
    simulation.step();
    console.log(simulation.state);
}

const v = simulation.state.velocity;
const impact_velocity = Math.sqrt(v.x * v.x  +  v.y * v.y)
if (impact_velocity > 1) {
  console.log('Your impact velocity was higher than 1.0 m/s. Your craft has crashed. There are no survivors.')
} else {
  console.log('congratulations, you have safely landed')
}
