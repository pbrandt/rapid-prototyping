const rp = require('../../')
const Simulation = rp.Simulation;
const joystick = new rp.Joystick()

const MAX_THRUST = 6;

const simulation = new Simulation();
simulation.state = {
    position: {
        x: 0,
        y: 1000
    },
    velocity: {
        x: 2,
        y: 0
    },
    control: {
        throttle: 0,
        angle: 0 // 0 is straight up, and the angles go clockwise because this is aerospace engineering?
    }
};

simulation.derivatives.add('first order', {
    position: {
        x(state, t) { return state.velocity.x },
        y(state, t) { return state.velocity.y }
    },
});

simulation.derivatives.add('gravity', {
    velocity: {
        y(state, t) { return -1.665; }
    }
});

simulation.derivatives.add('thrust', {
    velocity: {
        x(state, t) {
            return state.control.throttle * MAX_THRUST * Math.sin(state.control.angle * Math.PI / 180)
        },
        y(state, t) {
            return state.control.throttle * MAX_THRUST * Math.cos(state.control.angle * Math.PI / 180)
        }
    }
})

async function main() {
    var interface = await simulation.start_interface(8080)
    interface.plot('position.x', 'position.y')
    interface.plot('t', 'control.throttle')
    interface.plot('t', 'control.angle')

    setInterval(() => {
        console.log(`thrust: ${simulation.state.control.throttle} angle: ${simulation.state.control.angle} x: ${r(simulation.state.position.x)} y: ${r(simulation.state.position.y)} vx: ${r(simulation.state.velocity.x)} vy: ${r(simulation.state.velocity.y)}`)
    }, 500)
  
    // run the sim
    while (simulation.state.position.y > 0) {
        // set controls
        simulation.state.control.angle = joystick.roll;
        simulation.state.control.throttle = joystick.buttons[0] || 0;

        simulation.step_realtime();

        // allow backgronud processing
        await sleep(1) // 1 ms
    }

    const v = simulation.state.velocity;
    const impact_velocity = Math.sqrt(v.x * v.x  +  v.y * v.y)
    if (impact_velocity > 2) {
    console.log('Your impact velocity was higher than 2.0 m/s. Your craft has crashed. There are no survivors.')
    } else {
    console.log('congratulations, you have safely landed')
    }
}

async function sleep(ms) {
    return new Promise(r => {
        setTimeout(r, ms)
    })
}

function r(number) {
    return number.toFixed(1);
}

main()
