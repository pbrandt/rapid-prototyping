var rp = require('..')
var sim = new rp.Simulatino()

var state = {
  first_cannonball: {
    x: 0,
    y: 0,
    vx: 150 * Math.cos(Math.PI / 2),
    vy: 150 * Math.sin(Math.PI / 2)
  },
  second_cannonball: {
    x: 0,
    y: 0,
    vx: 150 * Math.cos(Math.PI / 3),
    vy: 150 * Math.sin(Math.PI / 3)
  }
}


var deriv = {
  first_cannonball: {
    vy: -9.81
  },
  second_cannonball: {
    vy: -9.81
  }
}



