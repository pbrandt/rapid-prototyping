var shared_raw_joystick = null

class Joystick {
  constructor () {
    if (!shared_raw_joystick) {
      shared_raw_joystick = new (require('joystick'))(0, 3500, 350)
    }
    this.pitch = 0
    this.roll = 0
    this.yaw = 0
    this.buttons = []

    const config = DEFAULT_CONFIG

    shared_raw_joystick.on('axis', data => {
      const val = data.value
      const axis = data.number

      var relative_val = 20 * val / (config.AXIS_MAX[axis] - config.AXIS_MIN[axis])

      if (isNaN(relative_val)) {
        console.log('nan', val, axis, config)
      }

      if (axis == config.PITCH) {
        this.pitch = relative_val
      } else if (axis == config.ROLL) {
        this.roll = relative_val
      } else if (axis == config.YAW) {
        this.yaw = relative_val
      }
    })

    shared_raw_joystick.on('button', data => {
      this.buttons[data.number] = data.value;
    })
  }


  get controls () {
    return {
      pitch: this.pitch,
      roll: this.roll,
      yaw: this.yaw
    }
  }
}

const DEFAULT_CONFIG = {
  PITCH: 1,
  ROLL: 0,
  YAW: 2,

  AXIS_MAX: [3000, 3000, 3000, 3000, 3000, 3000],
  AXIS_MIN: [-3000, -3000, -3000, -3000, -3000, -3000]
}

module.exports = Joystick
