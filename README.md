# Rapid prototyping

physics simulation that's doesn't try to do anything fancy, it just gives you a pretty simple sandbox. web interface included.

TODO publish package...

```
npm i rapid-prototyping
```

You have a `state` javascript object, and a corresponding `derivative` object. Observe a two-d lunar lander.

```js
const Simulation = require('rapid-prototyping').Simulation;

const simulation = new Simulation();
simulation.state = {
    position: {
        x: 0,
        y: 1000
    },
    velocity: {
        x: 2,
        y: 0
    },
    control: {
        throttle: 0,
        angle: 0 // 0 is straight up, and the angles go clockwise because this is aerospace engineering?
    }
};

// Add a bunch of derivatives to your stage. tag them if you would like. They're each called with two arguments: state and simulation time.
simulation.derivatives.add('first order', {
    position: {
        x(state, t) { return state.velocity.x },
        y(state, t) { return state.velocity.y }
    },
});

simulation.derivatives.add('gravity', {
    velocity: {
        y(state, t) { return -1.625; }
    }
});

simulation.derivatives.add('thrust', {
    velocity: {
        x(state, t) {
            return state.control.throttle * Math.sin(state.control.angle * Math.PI / 180)
        },
        y(state, t) {
            return -1 * state.control.throttle * Math.cos(state.control.angle * Math.PI / 180)
        }
    }
})

// run the sim
while (simulation.state.position.y > 0) {

    // first zero out horizontal velocity
    if (simulation.state.velocity.x > 0.01) {
        simulation.state.control.angle = -90; // turn opposte 90 degrees
        simulation.state.control.throttle = 1.0;
    }

    // then burn at the exact right time to zero out vertical velocity
    else if (false) {
        simulation.state.controls.angle = 90;
    } else {
      simulation.state.control.throttle = 0;
    }
    console.log('stepping')
    debugger;
    simulation.step();
    console.log(simulation.state);
}

```

If you've never done this before, your state is the position AND velocity, then your derivative should be velocity and acceleration. You get acceleration from doing a force body diagram. The derivative is usually a nonlinear function of the state and time.


## visualizations

the web ui can be used by doing this:

```js
simulation.start_interface(8080)
```

## joystick and realtime mode

todo