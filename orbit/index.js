
/**
 * Checks to see if the given object has all the specified keys or not
 * @param {*} opts pojo
 * @param {*} keys list of properties that opts should have
 */
function has_all (opts, keys) {
  return keys.reduce((rollup, key) => {
    return rollup && typeof opts[key] !== 'undefined'
  }, true)
}

/**
 * Orbit class that builds an orbit with all the right derivatives for you
 */
class Orbit {
  constructor (opts) {
    // all orbits must have options and a specified body
    if (typeof opts === 'undefined' || typeof opts.body === 'undefined') {
      throw new Error('Must specify orbit options. Example: new Orbit({body: "moon", altitude: 100, inclination: 28})')
    }

    // build an orbit with some set of values
    if (has_all(opts, ['altitude', 'inclination'])) {

    } else if (has_all(opts, ['a', 'e', 'i', 'raan', 'aop', 'ta'])) {
      // keplerian
      throw new Error('Keplerian elements not implemented yet')
    } else if (has_all(opts, ['x', 'y', 'z', 'vx', 'vy', 'vz'])) {
      // body-centered inertial
      throw new Error('Body-centered intertial frame not implemented yet')
    }

    if (typeof opts.altitude === 'undefined' || typeof opts.inclination === 'undefined') {
      throw new Error('Must specify orbit options. Example: new Orbit({body: "moon", altitude: 100, inclination: 28})')
    }
  }
}
