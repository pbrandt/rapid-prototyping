var Simulation = require('../../').Simulation;
require('should')

describe('simulation framework', () => {
  describe('derivative objects', () => {
    it('should allow you to add a derivative for a state', () => {
      var sim = new Simulation()
      sim.state = {
        x: 0,
        y: 0
      };

      // untagged
      sim.derivatives.add({
        x: function(state, t) {
          return 0.5;
        }
      })

      sim.step()
      sim.state.x.should.equal(sim.t * 0.5);


      // tagged
      sim.derivatives.add('more', {
        x: function(state, t) {
          return -0.1
        }
      })

      sim.step()
      sim.state.x.should.equal(sim.t * 0.5 - sim.t/2 * 0.1)
    })

    it('should allow you to use arrays in the state defintion', () => {
      var sim = new Simulation()
      sim.state = {
        arr: [{x: 0}, {x: 0}]
      }
      sim.derivatives.add({
        arr: [{x: () => 0.1}, {x: () => 0.1}]
      })

      sim.step()
      sim.state.arr[0].x.should.equal(sim.t * 0.1)

    })

    it('should correctly give the state and time as arguments', () => {

    })
  })
})