const log = module.exports = function() {
    console.log.apply(console, arguments)
}

log.debug = function() {
    if (process.env.DEBUG) {
        console.log.apply(console, arguments)
    }
}