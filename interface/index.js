const express = require('express')
const log = require('../log')

class Interface {
    constructor(port, sim) {
        var self = this;
        this.sim = sim;
        this.plots = []

        const app = express()
        const http = require('http').Server(app);
        var io = require('socket.io')(http);
        this.io = io;

        // serve the react app
        app.use(express.static(__dirname + '/app/build'))

        // connect websockets
        io.on('connection', function(socket){
            console.log('a user connected');
            self.send_data(self.sim.data)
            self.plots.map(p => {
                io.emit('plot', p)
            })
        });

        this.ready = new Promise((resolve, reject) => {
            http.listen(port, () => {
                console.log('Simulation interface listening at http://localhost:' + port)
                resolve()
            })

            setInterval(() => {
                if (this.has_more_data) {
                    this.io.emit('moredata', this.next_data)
                    this.has_more_data = false
                }
            }, 1000 / 30)
        })
    }

    /**
     * sends all the data, like meant to be called at the end of a sim run
     * @param {object} data 
     */
    send_data(data) {
        log.debug('sending data')

        // this is for realt
        this.io.emit('data', data)
    }

    push_data(data) {
        this.has_more_data = true;
        this.next_data = data;
    }

    /**
     * tell the interface to plot attributes
     * @param {string} x 
     * @param {string|array[string]} y 
     */
    plot(x, y) {
        log.debug('called plot with', x, y)

        if (typeof x !== 'string') {
            throw new Error('x must be the string name of an attribute of your state. x wasn\'t a string, it was ' + x)
        }

        if (!obj_has_dotted_prop(this.sim.state, x) && x !== 't') {
            console.log('state was:', this.sim.state)
            throw new Error('x must be the string name of an attribute of your state. ' + x + ' was not in your state.')
        }

        if (typeof y === 'undefined' || (typeof y !== 'string' && !(y instanceof Array))) {
            throw new Error('y must be the string name of an attribute of you state, or an array of them')
        }

        this.io.emit('plot', {x, y})
        this.plots.push({x, y})

    }

    draw() {
        log.debug('called draw')
        this.io.emit('draw')
    }
}

function obj_has_dotted_prop(obj, prop) {
    console.log(obj, prop)
    var curr = obj;
    var ok = true;
    (prop || '').split('.').map(k => {
        if (typeof curr[k] === 'undefined') {
            ok = false;
        } else {
            curr = curr[k];
        }
    })
    return ok
}

module.exports = Interface