import React, { Component } from 'react';
import Plot from 'react-plotly.js';
import io from 'socket.io-client'
import logo from './logo.svg';
import './App.css';

const colors = [
  '#2B823A',
  '#2B4C6F',
  '#AA7D39',
  '#AA4139',
  '#80b1d3',
  '#fdb462',
  '#b3de69',
  '#fccde5',
  '#d9d9d9',
  '#bc80bd',
  '#ccebc5',
  '#ffed6f'
]

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      plots: [],
      config: {}
    }
  }

  componentDidMount() {
    const socket = io(`//${window.location.hostname}:8080`);
    socket.on('data', data => {
      console.log('got data')
      this.setState({ data })
    });

    socket.on('plot', plot => {
      console.log('got plot command')
      // function plots_equal(a, b) {
      //   return a.x === b.x && a.y === b.y
      // }

      // var should_plot = !this.state.plots.reduce((p, found) => plots_equal(plot, p) || found, false)
      // if (should_plot) this.state.plots.append(plot)
      this.setState(state => ({plots: [...state.plots, plot]}))
    })

    socket.on('moredata', data => {
      console.log('got more data')
      this.setState(state => {
        if (state.data.length > 500) {
          state.data = state.data.reduce((data, datapoint, i) => {
            if ((i+1) % 3 !== 0) {
              data.push(datapoint)
            }
            return data
          }, [])
        }
        return {data: state.data.concat(data)}
      })
    })
    
    socket.on('connect', () => {
      console.log('socket io connected')
      this.setState({
        data: [],
        plots: []
      })
    })

    socket.on('disconnect', () => {
      console.log('socket io disconnected :(')
    })
  }

  render() {
    var plots = this.state.plots.map((p, i) => {
      if (!(p.y instanceof Array)) {
        p.y = [p.y]
      }

      var data = p.y.map((y, i) => ({
          x: this.state.data.map(x => p.x === 't' ? x.t : get(x.state, p.x)),
          y: this.state.data.map(x => get(x.state, y)),
          type: 'scatter',
          mode: 'lines+points',
          marker: {color: colors[i]},
          name: y
        }))

      return (<Plot
        key={i}
        data={data}
        config={this.state.config}
        layout={ {title: `${p.y} vs ${p.x}`, xaxis: {title: p.x}, yaxis: {title: p.y.join(', ')} } }
        />
      )

    })

    var viz

    return (
      <div className="App">
        <header className="App-header">
          <span>menu</span><span>another menu</span>
        </header>
        <div className="content">
          <div className="plot">
            {plots}
          </div>
          <div className="picker">
          </div>
        </div>
      </div>
    );
  }
}

export default App;

function get(obj, prop) {
  var curr = obj;
  var lost = false;
  (prop || '').split('.').map(p => {
    if (typeof curr[p] !== 'undefined') {
      curr = curr[p];
    } else {
      curr = {}
      lost = true
    }
  })
  if (lost) {
    return
  } else {
    return curr
  }
}